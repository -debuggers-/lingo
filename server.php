<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
include 'Lingo.php';

if($_SERVER['REQUEST_METHOD'] !== 'POST')
{
	die;
}

if(isset($_SESSION['LINGO']) && ! empty($_SESSION['LINGO']))
{
	$lingo = unserialize($_SESSION['LINGO']);
}

$request = isset($_POST['request']) ? $_POST['request'] : null;

switch($request)
{
	// Starts new Lingo game
	case 'start':
		$lingo = new Lingo($_POST['size']);
	break;

	// Submits input word
	case 'submit':
		$lingo->submit($_POST['input']);
	break;

	// Gets next word
	case 'next':
		$lingo->next();
	break;

	// Saves score
	case 'save':
		if($lingo->state() === -1)
		{
			$score = $lingo->score();
			$html = $lingo->render(true);
			unset($_SESSION['LINGO']);
			unset($lingo);

			die(json_encode([
				'html' => $html,
				'score' => $score
			]));
		}
	break;
}

echo json_encode([
	'html' => $lingo->render(),
	'state' => $lingo->state()
]);

$_SESSION['LINGO'] = serialize($lingo);
