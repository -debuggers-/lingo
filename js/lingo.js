var Lingo = (function(){

	var game;
	var size;

	/**
	 * Starts new game
	 *
	 */
	function init(tableSize) {
		game = document.getElementById('lingo');

		request('start', 'size='+tableSize, function(response) {
			size = tableSize;
			game.innerHTML = response.html;
		});
	}

	/**
	 * Gets next word
	 *
	 */
	function next() {
		request('next', null, function(response) {
			game.innerHTML = response.html;
		});
	}

	/**
	 * Submits input word
	 *
	 */
	function submit(input) {
		request('submit', 'input='+input, function(response) {
			game.innerHTML = response.html;
			switch(response.state){
				case 0:
					setTimeout(function() {
						next();
					}, 2000);
				break;
				case 1:
				break;
				case -1:
					setTimeout(function() {
						save();
					}, 2000);
				break;
			}
		});
	}

	/**
	 * Saves game result
	 *
	 */
	function save() {
		request('save', null, function(response) {
			game.innerHTML = response.html;
		});
	}

	/**
	 * Performs AJAX request
	 *
	 */
	function request(request, params, callback) {
		var xmlhttp;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		}

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				callback(JSON.parse(xmlhttp.responseText));
				if (game.querySelector('form')) {
					game.querySelector('form').addEventListener('submit', onFormSubmit);
					game.querySelector('form input').focus();
				}
			}
		}

		xmlhttp.open('POST', 'server.php', true);
		xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
		xmlhttp.send('request='+request+'&'+params);
	}

	/**
	 * Handles form submit event
	 *
	 */
	function onFormSubmit(e) {
		e.preventDefault();
		var input = document.querySelector('input').value;
		if (input.length < size) {
			return;
		}
		submit(input);
	}

	/**
	 * Returns public methods
	 *
	 */	
	return {
		init: function(size) {
			init(size);
		},
		submit: submit,
		next: next
	};

}());
