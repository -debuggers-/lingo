<?php

/**
 * Lingo
 *
 */
class Lingo
{

    /**
     * Size of Lingo game 
     *
     * @var int
     */
    private $size;

    /**
     * Game score
     *
     * @var int
     */
    private $score;

    /**
     * Number of guessed words
     *
     * @var int
     */
    private $counter;

    /**
     * Health vlue
     *
     * @var int
     */
    private $health;

    /**
     * Array of letters of current word
     *
     * @var array
     */    
    private $word;

    /**
     * Array of available words
     *
     * @var array
     */ 
    private $words;

    /**
     * Game state (-1, 0, or 1)
     *
     * @var int
     */    
    private $state;

    /**
     * Array of input words 
     *
     * @var array
     */
    private $input;


    /**
     * Constructs Lingo game object
     *
     * @access public
     * @param integer $size
     */
    public function __construct($size = 5)
    {
        if ( ! in_array($size, [4, 5, 6, 7]))
        {
            throw new Exception('Worng Lingo size value.');
        }

        // Sets game variables
        $this->size = intval($size);
        $this->score = 0;
        $this->counter = 0;
        $this->health = 1;
        $this->score = 0;
        $this->state = 1;
        $this->input = [];
        $this->words = [];

        // Gets random word
        $this->generateWord();
    }


    /**
     * Renders Lingo table output
     *
     * @access public
     * @return string
     */
    public function render($end = false)
    {
        if ($end)
        {
            return '<h1>Lingo</h1>
                <p>Paldies par spēli!</p>
                <p>Jūsu rezultāts: <strong>'.$this->counter.'</strong> atminēti vārdi / <strong>'.$this->score.'</strong> punkti</p>';
        }

        $html = '<table class="info">
            <tr>
                <td>'.$this->counter.'/'.$this->score.'</td>
                <td class="health">'.str_repeat('&hearts; ', $this->health).'</td>
            </tr>
        </table>';

        $html .= '<table class="guesses">';
        for ($i = 0; $i < $this->size; $i++)
        {
            if ( ! empty($this->input[$i]))
            {
                $html .= $this->createRow($this->input[$i], null, ($i == count($this->input)-1));
            }
            else
            {
                $html .= $this->createRow(null, (($i == 0) ? $this->word[0] : ''));
            }
        }
        $html .= '</table>';

        $html .= '<table class="input">';
        if ($this->state == 0 || count($this->input) == $this->size)
        {
            $html .= $this->createRow($this->word, null, true);
        }
        else
        {
            $html .= '<td>
                <form>
                    <input type="text" maxlength="'.$this->size.'"/>
                </form>
            </td>';
        }
        $html .= '</table>';
        
        return $html;
    }
    

    /**
     * Creates Lingo table row
     *
     * @access private
     * @param array|null $word
     * @param string $first
     * @param bool $animate
     * @return string
     */
    private function createRow($word = null, $first = '', $animate = false)
    {
        $html = '<tr>';
        for ($i = 0; $i < $this->size; $i++)
        {
            if ($word === null)
            {
                $html .= '<td>'.($i == 0 ? $first : '').'</td>';
            }
            else
            {
                $html .= '<td class="'.($animate ? 'animate ' : '').$this->letterStatus($word, $i).'">'.$word[$i].'</td>';
            }
        }
        $html .= '</tr>';

        return $html;
    }


    /**
     * Checks input word
     *
     * @access public
     * @param string $input
     */
    public function submit($input)
    {
        // Validates input value and game state
        if (empty($input) || ($this->state != 1) || count($this->input) == $this->size)
        {
            return;
        }

        $input = self::textToArray(strtolower($input));

        // Checks if the input word has valid size
        if (count($input) != $this->size)
        {
            return;
        }

        $this->input[] = $input;

        // Checks if the word is guessed
        if (end($this->input) === $this->word)
        {
            // Checks if word gussed at first time
            if (count($this->input) == 1)
            {
                // Adds bonus health point
                $this->health++;
            }
            $this->state = 0;
            $this->counter++;
            $this->updateScore();
        }
        else
        {
            // Checks if last attempt failed
            if (count($this->input) == $this->size)
            {
                $this->health--;
                $this->state = ($this->health == 0) ? -1 : 0;
            }
        }
    }


    /**
     * Generates next word
     *
     * @access public
     */
    public function next()
    {
        if($this->state == 0)
        {
            $this->state = 1;
            $this->input = [];
            $this->generateWord();
        }
    }


    /**
     * Checks for letter status
     *
     * @access private
     * @param string $input
     * @param integer $index
     * @return string
     */
    private function letterStatus($input, $index)
    {
        if($input[$index] == $this->word[$index])
        {
            $class = 'correct';
        }
        else
        {
            $class = (in_array($input[$index], $this->word))
                ? 'position'
                : 'wrong';
        }
        return $class;
    }


    /**
     * Generates random word
     *
     * @access private
     */
    private function generateWord()
    {
        if (empty($this->words))
        {
            $filename = 'data/lingo-'.$this->size.'.txt';
            if (file_exists($filename))
            {
                $this->words = file($filename, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
            }
        }

        $index = rand(0, count($this->words)-1);
        $this->word = self::textToArray($this->words[$index]);
        array_splice($this->words, $index, 1);
    }


    /**
     * Updates score depending on number of attempts
     *
     * @access private
     */
    private function updateScore()
    {
        $this->score += ((2 * $this->size) - 2 * (count($this->input)-1));
    }


    /**
     * Gets score value
     *
     * @access public
     * @return integer
     */
    public function score()
    {
        return $this->score;
    }


    /**
     * Gets game state
     *
     * @access public
     * @return integer
     */
    public function state()
    {
        return $this->state;
    }


    /**
     * Converts multibyte string into array of characters
     *
     * @access private
     * @return @array
     */
    private static function textToArray($text)
    {
        return preg_split("//u", $text, -1, PREG_SPLIT_NO_EMPTY);
    }


    /**
     * Magic method: __sleep
     *
     * @access private
     * @return array
     */
    public function __sleep()
    {
        return ['size', 'score', 'counter', 'health', 'word', 'words', 'input', 'state'];
    }

}
