<?php
session_start();
if( ! empty($_SESSION['LINGO'])){unset($_SESSION['LINGO']);}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title>Lingo</title>
	<link rel="stylesheet" href="css/lingo.css">
</head>
<body>

	<div id="lingo">
		<h1>Lingo</h1>
	    <p>Lingo ir spēle, kura tika izdomāta tālajā 1987. gadā ASV kā interaktīvā televīzijas spēle.</p>
	    <p>Šo spēli Latvijā savulaik ir popularizējusi Tildes datorvārdnīca.</p>
	    <p>Mēs piedāvājam tev uzspēlēt šo izglītojošo spēli internetā!</p>
	    <p>Noteikumi ir ļoti vienkārši: zinot pirmo vārda burtu un burtu skaitu tajā, tev vārds ir jāatmin, bet atceries, ka minējumu skaits ir ierobežots.</p>
	    <br />
	    <p>P.S.: Uzreiz atvainojamies par dažiem vārdiem :/</p>
	    <br />
		<p>Izvēlieties spēles līmeni, lai sāktu:</p>
		<nav>
			<input type="button" onclick="Lingo.init(4);" value="Mini">
			<input type="button" onclick="Lingo.init(5);" value="Classic">
			<input type="button" onclick="Lingo.init(6);" value="Large">
			<input type="button" onclick="Lingo.init(7);" value="Extra">
		</nav>
	</div>

	<script src="js/lingo.js"></script>
</body>
</html>
